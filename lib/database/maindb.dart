import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final _databaseName = "MyDatabase_ver10.db";
  static final _databaseVersion = 1;

  static final table = 'my_table';
  static final tableWater = 'table_water';

  static final columnId = '_id';
  static final columnStep = 'step';
  static final columnMinutes = 'minutes';
  static final columnSeconds = 'seconds';
  static final columnstepRange = 'stepRange';
  static final columnCcal = 'ccal';
  static final columnYear = 'year';
  static final columnMounth = 'mounth';
  static final columnDay = 'day';

  static final columnWaterId = '_id';
  static final columnWaterGlass = 'glass';
  static final columnWaterYear = 'year';
  static final columnWaterMounth = 'mounth';
  static final columnWaterDay = 'day';

  // Создаем синголтон
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  //
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    //Создаем экземпляр БД при первом обращении к нему
    _database = await _initDatabase();
    return _database;
  }

  // Открываем БД
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL код для создания таблицы
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY,
            $columnStep INTEGER NOT NULL,
            $columnMinutes INTEGER,
            $columnSeconds INTEGER,
            $columnstepRange INTEGER,
            $columnCcal INTEGER,
            $columnYear INTEGER NOT NULL,
            $columnMounth INTEGER NOT NULL, 
            $columnDay INTEGER NOT NULL
          )
         ''');
    await db.execute('''
          CREATE TABLE $tableWater (
            $columnWaterId INTEGER PRIMARY KEY,
            $columnWaterGlass INTEGER,
            $columnWaterYear INTEGER NOT NULL,
            $columnWaterMounth INTEGER NOT NULL,
            $columnWaterDay INTEGER NOT NULL
          )
         ''');
  }

  // Добавление записей
  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

   Future<int> insertWater(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tableWater, row);
  }

  //Метод который отображает всю таблицу в БД
  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    return await db.query(table);
  }

  //Метод который отображает всю таблицу в БД
  Future<List<Map<String, dynamic>>> queryAllRowsWater() async {
    Database db = await instance.database;
    return await db.query(tableWater);
  }

  //Метод запроса шагов за месяц
  Future<List<Map<String, dynamic>>> queryMonth(int day, int month) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(step) FROM $table WHERE mounth = $month AND day = $day');
  }

  //Метод запроса шагов за год
  Future<List<Map<String, dynamic>>> queryYear(int month, int year) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(step) FROM $table WHERE mounth = $month AND year = $year');
  }

//Метод запроса суммы ккал за месяц
  Future<List<Map<String, dynamic>>> queryMonthCcal(int day, int month) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(ccal) FROM $table WHERE mounth = $month AND day = $day');
  }

  //Метод запроса суммы ккал за год
  Future<List<Map<String, dynamic>>> queryYearCcal(int month, int year) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(ccal) FROM $table WHERE mounth = $month AND year = $year');
  }

//Метод запроса сумма расстояния за месяц
  Future<List<Map<String, dynamic>>> queryMonthStepRange(
      int day, int month) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(stepRange) FROM $table WHERE mounth = $month AND day = $day');
  }

  //Метод запроса сумма расстояния за год
  Future<List<Map<String, dynamic>>> queryYearStepRange(
      int month, int year) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(stepRange) FROM $table WHERE mounth = $month AND year = $year');
  }

  //Метод запроса колличества строк в таблице.
  Future<int> queryRowCount() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $table'));
  }

//Метод запроса сумма выпитой воды за 1 день при инициализации приложения
  Future<List<Map<String, dynamic>>> queryDayWater(
      int day, int month) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(glass) FROM $tableWater WHERE mounth = $month AND day = $day');
  }

  //Метод запроса сумма выпитой воды за месяц 
  Future<List<Map<String, dynamic>>> queryMonthGlassOfWater(
      int day, int month) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(glass) FROM $tableWater WHERE mounth = $month AND day = $day');
  }

  //Метод запроса сумма выпитой воды за год
  Future<List<Map<String, dynamic>>> queryYearGlassOfWater(
      int month, int year) async {
    Database db = await instance.database;
    return await db.rawQuery(
        'SELECT SUM(glass) FROM $tableWater WHERE mounth = $month AND year = $year');
  }

  //Метод удаления строки из таблицы TableWater
  Future<int> deleteRowInWaterTable(int id) async {
    Database db = await instance.database;
    return await db.delete(tableWater, where: '$columnWaterId = ?', whereArgs: [id]);
  }

  //Метод поиска последний строки в таблице TableWater
  Future<int> searchLastRowInWaterTable() async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableWater'));
  }
}
