import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_pedometer/flutter_pedometer.dart';
import 'database/maindb.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:app/pages/charts_page.dart';
import 'package:app/pages/ccal_charts_page.dart';
import 'package:app/pages/stepRange_charts_pages.dart';
import 'package:app/pages/run.dart';
import 'package:app/pages/functionalTraining.dart';
import 'package:app/pages/treadMill.dart';
import 'package:app/pages/waterChartsPage.dart';

class Counter extends StatefulWidget {
  @override
  _Counter createState() => new _Counter();
}

class _Counter extends State<Counter> {
  StreamSubscription<int>
      _subscription; // Объявление потока для данных с датчика
  //Объявление переменых для отображение данных
  //String _stepRange = '0';
  String stepCountstr = '0 / 6000';
  String elapsedTime = '';
  double stepRange = 0;
  double ccal = 0;
  double humanWeight = 80.0;
  int startStep, updateStep, step, seconds, minutes = 0;
  int waterCounter = 0;
  int numberNorm = 0;
  int stopStep = 0;
  int differenceUpdate = 0;
  int glassOfWater = 0;
  //Подключение БД
  final dbHelper = DatabaseHelper.instance;
  //Объявление для анимации секундомера и шагомера
  final GlobalKey<AnimatedCircularChartState> _chartKey =
      new GlobalKey<AnimatedCircularChartState>();
  final _chartSize = const Size(250.0, 250.0);
  Color labelColor = Colors.blue;
  Stopwatch watch = new Stopwatch();
  Timer timer;
  //Объявление списков данных для графиков
  List<charts.Series<MonthCharts, DateTime>> _seriespieData;
  List<charts.Series<YearCharts, String>> _seriespieDataYear;
  //List<charts.Series<WeekCharts, DateTime>> _seriespieDataWeek;
  List<charts.Series<MonthCcalCharts, DateTime>> _seriespieDataCcalMonth;
  List<charts.Series<YearCcalCharts, String>> _seriespieDataCcalYear;
  List<charts.Series<MonthStepRangeCharts, DateTime>>
      _seriespieDataStepRangeMonth;
  List<charts.Series<YearStepRangeCharts, String>> _seriespieDataStepRangeYear;
  List<charts.Series<MonthGlassOfWaterCharts, DateTime>>
      _seriespieDataWaterOfGlassMonth;
  List<charts.Series<YearGlassOfWaterCharts, String>>
      _seriespieDataWaterOfGlassYear;
  var listDays = [];
  var listMonth = [];
  //var listWeek = [];
  var listCcalMonth = [];
  var listCcalYear = [];
  var listStepRangeMonth = [];
  var listStepRangeYear = [];
  var listGlassOfWaterMonth = [];
  var listGlassOfWaterYear = [];
  Map daysCard;
  Map monthCard;
  //Map weekCard;
  Map ccalMonthCard;
  Map ccalYearCard;
  Map stepRangeMonthCard;
  Map stepRangeYearCard;
  Map GlassOfWaterMonthCard;
  Map GlassOfWaterYearCard;
  //Время
  var realTime = DateTime.now();

  List<CircularStackEntry> _generateChartData(int min, int second) {
    double temp = second * 0.6;
    double adjustedSeconds = second + temp;
    double tempmin = min * 0.6;
    double adjustedMinutes = min + tempmin;

    Color dialColor = Colors.blue;

    labelColor = dialColor;

    List<CircularStackEntry> data = [
      new CircularStackEntry(
          [new CircularSegmentEntry(adjustedSeconds, dialColor)])
    ];

    if (min > 0) {
      labelColor = Colors.green;
      data.removeAt(0);
      data.add(new CircularStackEntry(
          [new CircularSegmentEntry(adjustedSeconds, dialColor)]));
      data.add(new CircularStackEntry(
          [new CircularSegmentEntry(adjustedMinutes, Colors.green)]));
    }

    return data;
  }

  //UpdateTime функция отвечает за обновление анамации секундомера/времени/шагов
  updateTime(Timer timer) {
    if (watch.isRunning) {
      var milliseconds = watch.elapsedMilliseconds;
      int hundreds = (milliseconds / 10).truncate();
      seconds = (hundreds / 100).truncate();
      minutes = (seconds / 60).truncate();
      if (startStep == null) startStep = updateStep;
      step = updateStep - startStep - differenceUpdate;
      addditionalInformation(step);
      setState(() {
        elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
        stepCountstr = "$step / 6000";
        //_stepRange = "$stepRange";
        if (seconds > 59) {
          seconds = seconds - (59 * minutes);
          seconds = seconds - minutes;
        }
        List<CircularStackEntry> data = _generateChartData(minutes, seconds);
        _chartKey.currentState.updateData(data);
      });
    }
  }

//Функция запуска секундомера/шагомера
  startWatch() {
    if (stopStep != 0) differenceUpdate = updateStep - stopStep;
    watch.start();
    timer = new Timer.periodic(new Duration(milliseconds: 1000), updateTime);
  }

//Остановка всего
  stopWatch() {
    watch.stop();
    setTime();
    stopStep = updateStep;
  }

//Сброс всего  и запись в БД
  resetWatch() {
    _insert(step, seconds, minutes, ccal, stepRange);
    watch.reset();
    setTime();
    nullVariables();
  }

//Запись времени/шагов в круговую анимацию
  setTime() {
    var timeSoFar = watch.elapsedMilliseconds;
    setState(() {
      elapsedTime = transformMilliSeconds(timeSoFar);
      stepCountstr = "$step / 6000";
      List<CircularStackEntry> data = _generateChartData(0, 0);
      _chartKey.currentState.updateData(data);
    });
  }

//Транформация милисекунд
  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    seconds = (hundreds / 100).truncate();
    minutes = (seconds / 60).truncate();

    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$minutesStr:$secondsStr";
  }

// Подсчет дополнительной информации Ккал/Расстояния
  void addditionalInformation(int step) {
    stepRange = step / 2;
    ccal = 0.5 * humanWeight * ((step ~/ 2) / 1000.0);
  }

  minusGlassOfWater() {
    _delete();
    if (glassOfWater > 0) {
      setState(() {
        glassOfWater = glassOfWater - 1;
      });
    }
  }

  plusGlassOfWater() {
    int glass = 1;
    _insertWaterTable(glass);
    setState(() {
      glassOfWater = glassOfWater + 1;
    });
  }

  void initWater() async {
    final allRows = await dbHelper.queryDayWater(realTime.day, realTime.month);
    Map dayGlassOfWaterCard = allRows[0];
    int dayGlass = dayGlassOfWaterCard['SUM(glass)'];
    setState(() {
      if (dayGlass != null) {
        glassOfWater = dayGlass;
      } else
        glassOfWater = 0;
    });
  }

//Обнуление переменных
  void nullVariables() {
    startStep = null;
    stopStep = 0;
    differenceUpdate = 0;
    step = 0;
    ccal = 0.0;
    stepRange = 0.0;
    setState(() {
      //_stepRange = '$stepRange';
      stepCountstr = '0 / 6000';
    });
  }

//Сбор данных для графиков ккал/расстояние и вода за месяц и год
  void additionalInfoGraphics() async {
    //данные растояния
    var year = realTime.year.toInt();
    for (int month = 1; month <= 12; month++) {
      final allRows = await dbHelper.queryYearStepRange(month, year);
      stepRangeYearCard = allRows[0];
      listStepRangeYear.add(stepRangeYearCard['SUM(stepRange)']);
    }
    var yearDataStepRange = [
      YearStepRangeCharts('1', listStepRangeYear[0]),
      YearStepRangeCharts('2', listStepRangeYear[1]),
      YearStepRangeCharts('3', listStepRangeYear[2]),
      YearStepRangeCharts('4', listStepRangeYear[3]),
      YearStepRangeCharts('5', listStepRangeYear[4]),
      YearStepRangeCharts('6', listStepRangeYear[5]),
      YearStepRangeCharts('7', listStepRangeYear[6]),
      YearStepRangeCharts('8', listStepRangeYear[7]),
      YearStepRangeCharts('9', listStepRangeYear[8]),
      YearStepRangeCharts('10', listStepRangeYear[9]),
      YearStepRangeCharts('11', listStepRangeYear[10]),
      YearStepRangeCharts('12', listStepRangeYear[11])
    ];
    _seriespieDataStepRangeYear.add(charts.Series(
      data: yearDataStepRange,
      domainFn: (YearStepRangeCharts yearsteprangecharts, _) =>
          yearsteprangecharts.month,
      measureFn: (YearStepRangeCharts yearsteprangecharts, _) =>
          yearsteprangecharts.stepRange,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'YearStepRange',
    ));

    var month = realTime.month.toInt();
    for (int day = 1; day <= 31; day++) {
      final allRows = await dbHelper.queryMonthStepRange(day, month);
      stepRangeMonthCard = allRows[0];
      listStepRangeMonth.add(stepRangeMonthCard['SUM(stepRange)']);
    }
    var monthDataStepRange = [
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 1), listStepRangeMonth[0]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 2), listStepRangeMonth[1]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 3), listStepRangeMonth[2]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 4), listStepRangeMonth[3]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 5), listStepRangeMonth[4]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 6), listStepRangeMonth[5]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 7), listStepRangeMonth[6]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 8), listStepRangeMonth[7]),
      new MonthStepRangeCharts(
          new DateTime(realTime.year.toInt(), month, 9), listStepRangeMonth[8]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 10),
          listStepRangeMonth[9]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 11),
          listStepRangeMonth[10]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 12),
          listStepRangeMonth[11]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 13),
          listStepRangeMonth[12]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 14),
          listStepRangeMonth[13]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 15),
          listStepRangeMonth[14]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 16),
          listStepRangeMonth[15]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 17),
          listStepRangeMonth[16]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 18),
          listStepRangeMonth[17]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 19),
          listStepRangeMonth[18]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 20),
          listStepRangeMonth[19]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 21),
          listStepRangeMonth[20]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 22),
          listStepRangeMonth[21]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 23),
          listStepRangeMonth[22]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 24),
          listStepRangeMonth[23]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 25),
          listStepRangeMonth[24]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 26),
          listStepRangeMonth[25]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 27),
          listStepRangeMonth[26]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 28),
          listStepRangeMonth[27]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 29),
          listStepRangeMonth[28]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 30),
          listStepRangeMonth[29]),
      new MonthStepRangeCharts(new DateTime(realTime.year.toInt(), month, 31),
          listStepRangeMonth[30]),
    ];

    _seriespieDataStepRangeMonth.add(charts.Series(
      data: monthDataStepRange,
      domainFn: (MonthStepRangeCharts mothsteprangecharts, _) =>
          mothsteprangecharts.time,
      measureFn: (MonthStepRangeCharts mothsteprangecharts, _) =>
          mothsteprangecharts.stepRange,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'MonthStepRange',
    ));

    //данные ккал

    for (int month = 1; month <= 12; month++) {
      final allRows = await dbHelper.queryYearCcal(month, year);
      ccalYearCard = allRows[0];
      listCcalYear.add(ccalYearCard['SUM(ccal)']);
    }
    var yearDataCcal = [
      YearCcalCharts('1', listCcalYear[0]),
      YearCcalCharts('2', listCcalYear[1]),
      YearCcalCharts('3', listCcalYear[2]),
      YearCcalCharts('4', listCcalYear[3]),
      YearCcalCharts('5', listCcalYear[4]),
      YearCcalCharts('6', listCcalYear[5]),
      YearCcalCharts('7', listCcalYear[6]),
      YearCcalCharts('8', listCcalYear[7]),
      YearCcalCharts('9', listCcalYear[8]),
      YearCcalCharts('10', listCcalYear[9]),
      YearCcalCharts('11', listCcalYear[10]),
      YearCcalCharts('12', listCcalYear[11])
    ];
    _seriespieDataCcalYear.add(charts.Series(
      data: yearDataCcal,
      domainFn: (YearCcalCharts yearccalcharts, _) => yearccalcharts.month,
      measureFn: (YearCcalCharts yearccalcharts, _) => yearccalcharts.ccal,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'YearCcal',
    ));

    for (int day = 1; day <= 31; day++) {
      final allRows = await dbHelper.queryMonthCcal(day, month);
      ccalMonthCard = allRows[0];
      listCcalMonth.add(ccalMonthCard['SUM(ccal)']);
    }
    var monthDataCcal = [
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 1), listCcalMonth[0]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 2), listCcalMonth[1]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 3), listCcalMonth[2]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 4), listCcalMonth[3]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 5), listCcalMonth[4]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 6), listCcalMonth[5]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 7), listCcalMonth[6]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 8), listCcalMonth[7]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 9), listCcalMonth[8]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 10), listCcalMonth[9]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 11), listCcalMonth[10]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 12), listCcalMonth[11]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 13), listCcalMonth[12]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 14), listCcalMonth[13]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 15), listCcalMonth[14]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 16), listCcalMonth[15]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 17), listCcalMonth[16]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 18), listCcalMonth[17]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 19), listCcalMonth[18]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 20), listCcalMonth[19]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 21), listCcalMonth[20]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 22), listCcalMonth[21]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 23), listCcalMonth[22]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 24), listCcalMonth[23]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 25), listCcalMonth[24]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 26), listCcalMonth[25]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 27), listCcalMonth[26]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 28), listCcalMonth[27]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 29), listCcalMonth[28]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 30), listCcalMonth[29]),
      new MonthCcalCharts(
          new DateTime(realTime.year.toInt(), month, 31), listCcalMonth[30]),
    ];

    _seriespieDataCcalMonth.add(charts.Series(
      data: monthDataCcal,
      domainFn: (MonthCcalCharts mothsteprangecharts, _) =>
          mothsteprangecharts.time,
      measureFn: (MonthCcalCharts mothsteprangecharts, _) =>
          mothsteprangecharts.ccal,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'DaysCcal',
    ));

    //данные сколько выпил воды

    for (int month = 1; month <= 12; month++) {
      final allRows = await dbHelper.queryYearGlassOfWater(month, year);
      GlassOfWaterYearCard = allRows[0];
      listGlassOfWaterYear.add(GlassOfWaterYearCard['SUM(glass)']);
    }
    var yearDataGlassOfWater = [
      YearGlassOfWaterCharts('1', listGlassOfWaterYear[0]),
      YearGlassOfWaterCharts('2', listGlassOfWaterYear[1]),
      YearGlassOfWaterCharts('3', listGlassOfWaterYear[2]),
      YearGlassOfWaterCharts('4', listGlassOfWaterYear[3]),
      YearGlassOfWaterCharts('5', listGlassOfWaterYear[4]),
      YearGlassOfWaterCharts('6', listGlassOfWaterYear[5]),
      YearGlassOfWaterCharts('7', listGlassOfWaterYear[6]),
      YearGlassOfWaterCharts('8', listGlassOfWaterYear[7]),
      YearGlassOfWaterCharts('9', listGlassOfWaterYear[8]),
      YearGlassOfWaterCharts('10', listGlassOfWaterYear[9]),
      YearGlassOfWaterCharts('11', listGlassOfWaterYear[10]),
      YearGlassOfWaterCharts('12', listGlassOfWaterYear[11])
    ];
    _seriespieDataWaterOfGlassYear.add(charts.Series(
      data: yearDataGlassOfWater,
      domainFn: (YearGlassOfWaterCharts yearglassofwatercharts, _) =>
          yearglassofwatercharts.month,
      measureFn: (YearGlassOfWaterCharts yearglassofwatercharts, _) =>
          yearglassofwatercharts.glassOfWater,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'YearGlassOfWater',
    ));

    for (int day = 1; day <= 31; day++) {
      final allRows = await dbHelper.queryMonthGlassOfWater(day, month);
      GlassOfWaterMonthCard = allRows[0];
      if (GlassOfWaterMonthCard['SUM(glass)'] !=
          null) if (GlassOfWaterMonthCard['SUM(glass)'] >= 8) waterCounter++;
      listGlassOfWaterMonth.add(GlassOfWaterMonthCard['SUM(glass)']);
    }
    var monthGlassOfWater = [
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 1),
          listGlassOfWaterMonth[0]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 2),
          listGlassOfWaterMonth[1]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 3),
          listGlassOfWaterMonth[2]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 4),
          listGlassOfWaterMonth[3]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 5),
          listGlassOfWaterMonth[4]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 6),
          listGlassOfWaterMonth[5]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 7),
          listGlassOfWaterMonth[6]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 8),
          listGlassOfWaterMonth[7]),
      new MonthGlassOfWaterCharts(new DateTime(realTime.year.toInt(), month, 9),
          listGlassOfWaterMonth[8]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 10),
          listGlassOfWaterMonth[9]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 11),
          listGlassOfWaterMonth[10]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 12),
          listGlassOfWaterMonth[11]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 13),
          listGlassOfWaterMonth[12]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 14),
          listGlassOfWaterMonth[13]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 15),
          listGlassOfWaterMonth[14]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 16),
          listGlassOfWaterMonth[15]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 17),
          listGlassOfWaterMonth[16]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 18),
          listGlassOfWaterMonth[17]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 19),
          listGlassOfWaterMonth[18]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 20),
          listGlassOfWaterMonth[19]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 21),
          listGlassOfWaterMonth[20]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 22),
          listGlassOfWaterMonth[21]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 23),
          listGlassOfWaterMonth[22]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 24),
          listGlassOfWaterMonth[23]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 25),
          listGlassOfWaterMonth[24]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 26),
          listGlassOfWaterMonth[25]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 27),
          listGlassOfWaterMonth[26]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 28),
          listGlassOfWaterMonth[27]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 29),
          listGlassOfWaterMonth[28]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 30),
          listGlassOfWaterMonth[29]),
      new MonthGlassOfWaterCharts(
          new DateTime(realTime.year.toInt(), month, 31),
          listGlassOfWaterMonth[30]),
    ];

    _seriespieDataWaterOfGlassMonth.add(charts.Series(
      data: monthGlassOfWater,
      domainFn: (MonthGlassOfWaterCharts monthglassofwatercharts, _) =>
          monthglassofwatercharts.time,
      measureFn: (MonthGlassOfWaterCharts monthglassofwatercharts, _) =>
          monthglassofwatercharts.glassOfWater,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'MonthGlassOfWater',
    ));
  }

//Сбор данных для графиков за за год, неделю, месяц
  void annualPlan() async {
    var year = realTime.year.toInt();
    for (int month = 1; month <= 12; month++) {
      final allRows = await dbHelper.queryYear(month, year);
      monthCard = allRows[0];
      listMonth.add(monthCard['SUM(step)']);
    }
    var yearData = [
      YearCharts('1', listMonth[0]),
      YearCharts('2', listMonth[1]),
      YearCharts('3', listMonth[2]),
      YearCharts('4', listMonth[3]),
      YearCharts('5', listMonth[4]),
      YearCharts('6', listMonth[5]),
      YearCharts('7', listMonth[6]),
      YearCharts('8', listMonth[7]),
      YearCharts('9', listMonth[8]),
      YearCharts('10', listMonth[9]),
      YearCharts('11', listMonth[10]),
      YearCharts('12', listMonth[11])
    ];
    _seriespieDataYear.add(charts.Series(
      data: yearData,
      domainFn: (YearCharts yearcharts, _) => yearcharts.month,
      measureFn: (YearCharts yearcharts, _) => yearcharts.countStep,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'Month',
    ));

    var month = realTime.month.toInt();
    for (int day = 1; day <= 31; day++) {
      final allRows = await dbHelper.queryMonth(day, month);
      daysCard = allRows[0];
      if (daysCard['SUM(step)'] != null) if (daysCard['SUM(step)'] >= 6000)
        numberNorm++;
      listDays.add(daysCard['SUM(step)']);
    }
    var pieData = [
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 1), listDays[0]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 2), listDays[1]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 3), listDays[2]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 4), listDays[3]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 5), listDays[4]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 6), listDays[5]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 7), listDays[6]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 8), listDays[7]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 9), listDays[8]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 10), listDays[9]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 11), listDays[10]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 12), listDays[11]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 13), listDays[12]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 14), listDays[13]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 15), listDays[14]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 16), listDays[15]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 17), listDays[16]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 18), listDays[17]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 19), listDays[18]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 20), listDays[19]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 21), listDays[20]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 22), listDays[21]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 23), listDays[22]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 24), listDays[23]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 25), listDays[24]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 26), listDays[25]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 27), listDays[26]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 28), listDays[27]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 29), listDays[28]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 30), listDays[29]),
      new MonthCharts(
          new DateTime(realTime.year.toInt(), month, 31), listDays[30]),
    ];

    _seriespieData.add(charts.Series(
      data: pieData,
      domainFn: (MonthCharts mothcharts, _) => mothcharts.time,
      measureFn: (MonthCharts mothcharts, _) => mothcharts.countStep,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      id: 'Days',
    ));

    // var aWeekAgo = realTime.day.toInt();
    // aWeekAgo = aWeekAgo - 6;
    // var week = [];

    // for (int day = aWeekAgo; day <= realTime.day.toInt(); day++) {
    //   final allRows = await dbHelper.queryMonth(day, month);
    //   weekCard = allRows[0];
    //   //print(weekCard);
    //   listWeek.add(weekCard['SUM(step)']);
    //   //print(listWeek);
    //   week.add(day);
    // }
    // var weekData = [
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[0]), listWeek[0]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[1]), listWeek[1]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[2]), listWeek[2]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[3]), listWeek[3]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[4]), listWeek[4]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[5]), listWeek[5]),
    //   new WeekCharts(
    //       new DateTime(realTime.year.toInt(), month, week[6]), listWeek[6])
    // ];
    // _seriespieDataWeek.add(charts.Series(
    //   data: weekData,
    //   domainFn: (WeekCharts weekcharts, _) => weekcharts.day,
    //   measureFn: (WeekCharts weekcharts, _) => weekcharts.countStep,
    //   colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
    //   id: 'Week',
    // ));
  }

//Запускает функции при инициализации
  @override
  void initState() {
    super.initState();
    initPlatformState();
    //_seriespieDataWeek = List<charts.Series<WeekCharts, DateTime>>();
    _seriespieData = List<charts.Series<MonthCharts, DateTime>>();
    _seriespieDataYear = List<charts.Series<YearCharts, String>>();
    _seriespieDataCcalMonth = List<charts.Series<MonthCcalCharts, DateTime>>();
    _seriespieDataCcalYear = List<charts.Series<YearCcalCharts, String>>();
    _seriespieDataStepRangeMonth =
        List<charts.Series<MonthStepRangeCharts, DateTime>>();
    _seriespieDataStepRangeYear =
        List<charts.Series<YearStepRangeCharts, String>>();
    _seriespieDataWaterOfGlassMonth =
        List<charts.Series<MonthGlassOfWaterCharts, DateTime>>();
    _seriespieDataWaterOfGlassYear =
        List<charts.Series<YearGlassOfWaterCharts, String>>();
    additionalInfoGraphics();
    annualPlan();
    initWater();
  }

  // Асинхронных метод сбора данных с датчика шагомера
  Future<void> initPlatformState() async {
    FlutterPedometer pedometer = new FlutterPedometer();
    _subscription = pedometer.stepCountStream.listen(_onData,
        onError: _onError, onDone: _onDone, cancelOnError: true);

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  void _onData(int stepCountValue) async {
    updateStep = stepCountValue;
  }

  void _onDone() {}

  void _onError(error) {
    print("Flutter Pedometer Error: $error");
  }

//Функция записи в БД
  void _insert(int steps, int seconds, int minutes, double ccal,
      double stepRange) async {
    //_stepRange = "$stepRange";
    if (step != 0) {
      Map<String, dynamic> row = {
        DatabaseHelper.columnStep: steps,
        DatabaseHelper.columnMinutes: minutes,
        DatabaseHelper.columnSeconds: seconds,
        DatabaseHelper.columnCcal: ccal,
        DatabaseHelper.columnstepRange: stepRange,
        DatabaseHelper.columnYear: realTime.year.toInt(),
        DatabaseHelper.columnMounth: realTime.month.toInt(),
        DatabaseHelper.columnDay: realTime.day.toInt()
      };
      final id = await dbHelper.insert(row);
    }
  }

  void _insertWaterTable(int glassOfWater) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnWaterGlass: glassOfWater,
      DatabaseHelper.columnWaterYear: realTime.year.toInt(),
      DatabaseHelper.columnWaterMounth: realTime.month.toInt(),
      DatabaseHelper.columnWaterDay: realTime.day.toInt()
    };
    final id = await dbHelper.insertWater(row);
  }

  void _delete() async {
    final id = await dbHelper.searchLastRowInWaterTable();
    final rowsDeleted = await dbHelper.deleteRowInWaterTable(id);
    //print('deleted $rowsDeleted row(s): row $id');
  }

  @override
  Widget build(BuildContext context) {
    TextStyle _labelStyle = Theme.of(context)
        .textTheme
        .title
        .merge(new TextStyle(color: labelColor));
    return Container(
        padding: EdgeInsets.all(20.0),
        child: new Column(
          children: <Widget>[
            SizedBox(
              width: 335,
              height: 250,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/stopwatch.png'),
                  ),
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black87,
                      blurRadius: 4.0,
                    )
                  ],
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.orange[600],
                ),
                child: new AnimatedCircularChart(
                  key: _chartKey,
                  size: _chartSize,
                  initialChartData: _generateChartData(0, 0),
                  chartType: CircularChartType.Radial,
                  edgeStyle: SegmentEdgeStyle.round,
                  percentageValues: true,
                  holeLabel: stepCountstr,
                  labelStyle: _labelStyle,
                ),
              ),
            ),
            SizedBox(
              height: 52.0,
              width: 370.0,
              child: Container(
                margin: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.orange,
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black45,
                      blurRadius: 4.0,
                    )
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Center(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Расстояние:\n $stepRange м',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            elapsedTime,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Калории:\n $ccal ккал',
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new FloatingActionButton(
                    heroTag: 'START',
                    backgroundColor: Colors.orange,
                    onPressed: startWatch,
                    child: new Icon(Icons.play_arrow)),
                SizedBox(width: 20.0),
                new FloatingActionButton(
                    heroTag: 'PAUSE',
                    backgroundColor: Colors.deepOrangeAccent,
                    onPressed: stopWatch,
                    child: new Icon(Icons.stop)),
                SizedBox(width: 20.0),
                new FloatingActionButton(
                    heroTag: 'RESET',
                    backgroundColor: Colors.orange,
                    onPressed: resetWatch,
                    child: new Icon(Icons.refresh)),
              ],
            ),
            SizedBox(
              width: 335,
              height: 120,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 98,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.grey[300],
                      image: DecorationImage(
                        image: AssetImage('assets/images/icons/run.png'),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Тренировка бега',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => RunPage(),
                              ),
                            );
                          },
                          child: Text(''),
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: Container(
                      width: 99,
                      height: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.grey[300],
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/images/icons/functionalTraining.png'),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Функц. тренировка',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FunctionalTraining(),
                                ),
                              );
                            },
                            child: Text(''),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 20.0),
                  Expanded(
                    child: Container(
                      width: 98,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                          image:
                              AssetImage('assets/images/icons/treadMill.png'),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Беговая дорожка',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TreadMill(),
                                ),
                              );
                            },
                            child: Text(''),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 335,
              height: 100,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage('assets/images/icons/charts.png'),
                  ),
                ),
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChartsPage(
                            numberNorm, _seriespieData, _seriespieDataYear),
                      ),
                    );
                  },
                  child: Text(
                    'Статистика',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 335,
              height: 120,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 100,
                       alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: FlatButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CcalChartsPage(
                                    _seriespieDataCcalMonth,
                                    _seriespieDataCcalYear),
                              ),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(FontAwesomeIcons.fire),
                              
                              Text(
                                'Ккал',
                                style: TextStyle(fontSize: 20),
                              ),
                            ],
                          )),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                    height: 100,
                  ),
                  Expanded(
                    child: Container(
                      height: 100,
                       alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                        
                      ),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => StepRangeChartsPage(
                                  _seriespieDataStepRangeMonth,
                                  _seriespieDataStepRangeYear),
                            ),
                          );
                        },
                        child: Row(children: <Widget>[Icon(FontAwesomeIcons.walking),Text(
                          'Расстояние',
                          style: TextStyle(fontSize: 18),
                        ),],)
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 335,
              height: 100,
              child: Container(
                width: 335,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.center,
                          width: 100,
                          height: 40,
                          //color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(FontAwesomeIcons.glassWhiskey),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                'Вода',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 100,
                          height: 60,
                          alignment: Alignment.center,
                          child: Text(
                            '$glassOfWater ',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 40),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 135,
                      height: 100,
                      child: Container(
                        alignment: Alignment.center,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => WaterChartsPage(
                                    waterCounter,
                                    _seriespieDataWaterOfGlassMonth,
                                    _seriespieDataWaterOfGlassYear),
                              ),
                            );
                          },
                          child: Text(
                            'Статистика',
                            style: TextStyle(fontSize: 19),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 50,
                      height: 100,
                      child: Container(
                          alignment: Alignment.center,
                          child: IconButton(
                            icon: new Icon(FontAwesomeIcons.minusCircle),
                            onPressed: () {
                              minusGlassOfWater();
                            },
                          )),
                    ),
                    SizedBox(
                      width: 50,
                      height: 100,
                      child: Container(
                        alignment: Alignment.center,
                        child: IconButton(
                          icon: new Icon(FontAwesomeIcons.plusCircle),
                          onPressed: () {
                            plusGlassOfWater();
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
//3 класса для для графиков шагов
// class WeekCharts {
//   final DateTime day;
//   final int countStep;
//   WeekCharts(this.day, this.countStep);
// }

class MonthCharts {
  final DateTime time;
  final int countStep;
  MonthCharts(this.time, this.countStep);
}

class YearCharts {
  final String month;
  final int countStep;
  YearCharts(this.month, this.countStep);
}

//2 класса для для графиков сожжённых калорий
class MonthCcalCharts {
  final DateTime time;
  final double ccal;
  MonthCcalCharts(this.time, this.ccal);
}

class YearCcalCharts {
  final String month;
  final double ccal;
  YearCcalCharts(this.month, this.ccal);
}

//2 класса для для графиков пройденного расстояния
class MonthStepRangeCharts {
  final DateTime time;
  final double stepRange;
  MonthStepRangeCharts(this.time, this.stepRange);
}

class YearStepRangeCharts {
  final String month;
  final double stepRange;
  YearStepRangeCharts(this.month, this.stepRange);
}

//2 класса для для графиков выпитой воды
class MonthGlassOfWaterCharts {
  final DateTime time;
  final int glassOfWater;
  MonthGlassOfWaterCharts(this.time, this.glassOfWater);
}

class YearGlassOfWaterCharts {
  final String month;
  final int glassOfWater;
  YearGlassOfWaterCharts(this.month, this.glassOfWater);
}
