import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TreadMill extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Тренировки на беговой дорожке"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Бег на беговой дорожке тренирует сердечно-сосудистую и дыхательную систему. За счет этого улучшается кровообращение, уменьшается пульс в спокойном состоянии, что делает работу сердца более экономичной. Также увеличивается объем легких и их способность использовать кислород, за счет увеличения количества капилляров.',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
      ),
      body: new ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return new TreadMillTraining(listOfTiles[index]);
        },
        itemCount: listOfTiles.length,
      ),
    );
  }
}

class TreadMillTraining extends StatelessWidget {
  final ListTreadMillTraining listTreadMillTraining;
  TreadMillTraining(this.listTreadMillTraining);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(listTreadMillTraining);
  }

  Widget _buildTiles(ListTreadMillTraining root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<ListTreadMillTraining>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }
}

class ListTreadMillTraining {
  String title;
  List<ListTreadMillTraining> children;
  ListTreadMillTraining(this.title,
      [this.children = const <ListTreadMillTraining>[]]);
}

List<ListTreadMillTraining> listOfTiles = <ListTreadMillTraining>[
  new ListTreadMillTraining(
    'Программа занятий для похудения',
    <ListTreadMillTraining>[
      new ListTreadMillTraining(
        'Ходьба на беговой дорожке для похудения',
        <ListTreadMillTraining>[
          new ListTreadMillTraining('Ходьба – продолжительная физическая нагрузка в медленном темпе длительностью до часа. Сделайте наклон беговой дорожки 25% от максимального наклона, скорость 5 км/ч и начинайте свою тренировку. Время тренировки будет зависеть от усталости организма. '),
          
        ],
      ),
      new ListTreadMillTraining(
        'Бег на беговой дорожке для похудения',
        <ListTreadMillTraining>[
          new ListTreadMillTraining(
            'Длительный бег',
            <ListTreadMillTraining>[
              new ListTreadMillTraining(
                  'Длительный бег — продолжительная физическая нагрузка в медленном темпе длительностью до часа, при которой частота сердечных сокращений поддерживается на уровне 120-135 ударов в минуту. Если вы новичок, то вам будет утомительно сразу же брать высокую планку нагрузок, поэтому начните свои тренировки с небольшой длительности бега и постепенно увеличивайте нагрузку. Для новичка продолжительность тренировки должна составлять не менее 25 минут.'),
            ],
          ),
          new ListTreadMillTraining('Интервальный бег',
            <ListTreadMillTraining>[
              new ListTreadMillTraining(
                  'Интервального бега - чередовании интенсивного и спокойного темпов бега. Например, 2 минуты стремительного темпа, после чего переход на спокойный бег 3-5 минут, после вновь двухминутный интенсивный темп и снова переход на 3-5 минут спокойного темпа. Такая пробежка с переменой фаз должна продолжаться примерно до получаса. '),
            ],),
          
        ],
      ),
      
    ],
  ),
  new ListTreadMillTraining(
    'Программа занятий для тренировки сердечно-сосудистой системы и выносливости',
    <ListTreadMillTraining>[
      new ListTreadMillTraining('1. Разминка 5 минут (начинается с ходьбы, постепенно переходя на легкий бег).'),
      new ListTreadMillTraining('2. Бег в спокойном или среднем темпе (2 минуты с определенной скоростью).'),
      new ListTreadMillTraining('3. Бег в интенсивном темпе (1 минуту с определенной скоростью).'),
      new ListTreadMillTraining('4. Бег в среднем темпе (2 минуты с определенной скоростью).'),
      new ListTreadMillTraining('5. Бег в интенсивном темпе (1 минуту с определенной скоростью).'),
      new ListTreadMillTraining('6. Многоразовый повтор бега в среднем и интенсивном темпе.'),
    ],
  ),
  new ListTreadMillTraining(
    'Программа занятий для тренировки мышц',
    <ListTreadMillTraining>[
      new ListTreadMillTraining('Основной стратегией для развития мышц ног является увеличение нагрузки. Нагрузку можно увеличить изменением наклона полотна и/или увеличением скорости движения полотна. Интенсивный бег под наклоном является лучшим средством тренировки мышц ног на беговой дорожке.'),  
    ],
  ),
];
