import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import '../step_counter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ChartsPage extends StatelessWidget {
  final int numberNorm;
  final List<charts.Series<MonthCharts, DateTime>> seriesList;
  final List<charts.Series<YearCharts, String>> _seriespieDataYear;
  final List<Tab> myTabs = [
    Tab(text: 'Информация'),
    Tab(text: 'Месяц'),
    Tab(text: 'Год')
  ];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ChartsPage(this.numberNorm,this.seriesList, this._seriespieDataYear, );
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          bottom: TabBar(
            tabs: myTabs,
          ),
          title: Text('Шаги'),
          actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Изучайте динамику количества шагов, чтобы улучшить результаты.',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
        ),
        body: TabBarView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 250,
                      alignment: Alignment.center,
                      child: new Image(
                        image: AssetImage('assets/images/run-icon.png'),
                      ),
                    ),
                    Text(
                      '$numberNorm',
                      style: TextStyle(fontSize: 100),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 300,
                      height: 110,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding (padding: EdgeInsets.all(10.0), child :Text(
                        'Количество дней, когда вы преодолели норму шагов в день. Следите за количество физической активности. Норма шагов 6000 в день.',
                        style: TextStyle(fontSize: 16),textAlign: TextAlign.justify,
                      ),),
                    )
                  ],
                ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Месяц',
                      style: TextStyle(fontSize: 14),
                    ),
                    Expanded(
                      child: new charts.TimeSeriesChart(
                        seriesList,
                        animate: true,
                        defaultRenderer:
                            new charts.BarRendererConfig<DateTime>(),
                        defaultInteractions: false,
                        behaviors: [
                          new charts.SelectNearest(),
                          new charts.DomainHighlighter()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Год',
                      style: TextStyle(fontSize: 14),
                    ),
                    Expanded(
                      child: new charts.BarChart(
                        _seriespieDataYear,
                        animate: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
