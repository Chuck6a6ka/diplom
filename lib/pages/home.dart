import 'package:flutter/material.dart';
import '../step_counter.dart';
import 'package:app/database/maindb.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

final dbHelper = DatabaseHelper.instance;
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

void _query() async {
  final allRows = await dbHelper.queryAllRows();
  print('query all rows:');
  allRows.forEach((row) => print(row));
}

void _queryWater() async {
  final allRows = await dbHelper.queryAllRowsWater();
  print('query all rows:');
  allRows.forEach((row) => print(row));
}

void _insert() async {
  var now = DateTime.now();

  Map<String, dynamic> row = {
    DatabaseHelper.columnStep: 6001,
    DatabaseHelper.columnMinutes: 3,
    DatabaseHelper.columnSeconds: 10,
    DatabaseHelper.columnCcal: 333.52,
    DatabaseHelper.columnstepRange: 3000.5,
    DatabaseHelper.columnYear: now.year.toInt(),
    DatabaseHelper.columnMounth: now.month.toInt(),
    DatabaseHelper.columnDay: 27
  };

  final id = await dbHelper.insert(row);
  print('inserted row id: $id');
}

void _insertWater() async {
  var now = DateTime.now();

  Map<String, dynamic> row = {
    DatabaseHelper.columnWaterGlass: 4,
    DatabaseHelper.columnWaterYear: now.year.toInt(),
    DatabaseHelper.columnWaterMounth: now.month.toInt(),
    DatabaseHelper.columnWaterDay: 28
  };

  final id = await dbHelper.insertWater(row);
  print('inserted row id: $id');
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget button = RaisedButton(
      child: Text(
        'query table',
        style: TextStyle(fontSize: 20),
      ),
      onPressed: () {
        _query();
      },
    );

    Widget button2 = RaisedButton(
      child: Text('Insert table'),
      onPressed: () {
        _insert();
      },
    );

     Widget button3 = RaisedButton(
      child: Text('Query tableWater'),
      onPressed: () {
        _queryWater();
      },
    );

     Widget button4 = RaisedButton(
      child: Text('Insert tableWater'),
      onPressed: () {
        _insertWater();
      },
    );

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Здоровье'),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'В данном окне приложения можно посмотреть полезную информацию про вашу физическую активность.Нажмите кнопку "Старт", чтобы начать, кнопка "Пауза", останавливает подсчет, кнопка "Сброс", сбрасывает данные и начинает считать заново."1" стакан воды это 250 мл.',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
      ),
      body: ListView(
        children: [
          //button,
          //button2,
          //button3,
          //button4,
          Counter(),
        ],
      ),
    );
  }
}
