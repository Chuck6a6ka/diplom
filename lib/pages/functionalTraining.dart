import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FunctionalTraining extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Функциональная тренировка"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Функциональная тренировка укрепляет весь мышечный корсет, особенно мышцы низа спины и живота. Постепенно улучшается осанка, чувство равновесия, контроль над движениями, общая выносливость и тонус организма. ',
                    textAlign: TextAlign.left,
                    
                    style: TextStyle(color: Colors.black, fontSize: 15) ,
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
      ),
      body: new ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return new FunTraining(listOfTiles[index]);
        },
        itemCount: listOfTiles.length,
      ),
    );
  }
}

class FunTraining extends StatelessWidget {
  final ListFunctionTraining listFunTraining;
  FunTraining(this.listFunTraining);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(listFunTraining);
  }

  Widget _buildTiles(ListFunctionTraining root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<ListFunctionTraining>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }
}

class ListFunctionTraining {
  String title;
  List<ListFunctionTraining> children;
  ListFunctionTraining(this.title,
      [this.children = const <ListFunctionTraining>[]]);
}

List<ListFunctionTraining> listOfTiles = <ListFunctionTraining>[
  new ListFunctionTraining(
    'Программа функциональной тренировки для мужчин.',
    <ListFunctionTraining>[
      new ListFunctionTraining(
        'Каждая тренировка длится не менее часа и начинается с разминки. Далее выполняются комплексы упражнений.',
      ),
      new ListFunctionTraining(
        'Понедельник',
        <ListFunctionTraining>[
          new ListFunctionTraining(
            '1. 15 подтягиваний в висе на перекладине.',
          ),
          new ListFunctionTraining('2. 10 приседаний.'),
          new ListFunctionTraining('3. 12 берпи.', <ListFunctionTraining>[
            new ListFunctionTraining(
              'Берпи – отожмитесь от пола, подтяните руки к груди, выпрыгните вверх и хлопните руками над головой',
            )
          ]),
          new ListFunctionTraining('4. Минута прыжков на скакалке.'),
          new ListFunctionTraining('5. Две минуты ускоренного бега.'),
          new ListFunctionTraining(
              'Комплекс рекомендуется выполнять в течение 15 минут, затем прерываться на 1 минуту и повторять упражнения.'),
        ],
      ),
      new ListFunctionTraining(
        'Вторник',
        <ListFunctionTraining>[
          new ListFunctionTraining('1. 15 приседаний.'),
          new ListFunctionTraining('2. 15 отжиманий на брусьях.'),
          new ListFunctionTraining(
              'Выполните 2–3 подхода с отдыхом 1–2 минуты.'),
        ],
      ),
      new ListFunctionTraining(
        'Среда',
        <ListFunctionTraining>[
          new ListFunctionTraining('1. 20 запрыгиваний на степ-платформу.'),
          new ListFunctionTraining('2. 15 подтягиваний на перекладине.'),
          new ListFunctionTraining('3. 15 приседаний.'),
          new ListFunctionTraining(
              '4. 24 выпада — 12 для правой ноги и 12 для левой.'),
          new ListFunctionTraining(
              'Выполняйте упражнения в течение 15 минут, затем отдохните 1 минуту и повторите. В завершение сделайте 50 боковых скручиваний — по 25 с каждый стороны.'),
        ],
      ),
      new ListFunctionTraining(
        'Четверг',
        <ListFunctionTraining>[
          new ListFunctionTraining('1. 12 отжиманий.'),
          new ListFunctionTraining(
              '2. Становая тяга с небольшим весом — 10 повторений.'),
          new ListFunctionTraining(
              '3. Толчки штанги над головой — 10 повторений.'),
          new ListFunctionTraining('4. Гиперэкстензия без веса — 10 повторений.'),
          new ListFunctionTraining('5. 3 минуты бега на месте.'),
          new ListFunctionTraining(
              'Этот комплекс необходимо повторить 3 раза, затем 2 раза сделать следующий комплекс: 25 подъемов ног в висе и 25 прямых скручиваний.'),
        ],
      ),
      new ListFunctionTraining(
        'Пятница',
        <ListFunctionTraining>[
          new ListFunctionTraining('1. 10 отжиманий.'),
          new ListFunctionTraining('2. 10 подтягиваний.'),
          new ListFunctionTraining('3. Повторите комплекс 5 раз.'),
          new ListFunctionTraining('4. Гиперэкстензия без веса — 10 повторений.'),
          new ListFunctionTraining('5. 3 минуты бега на месте.'),
          new ListFunctionTraining(
              'Этот комплекс необходимо повторить 3 раза, затем 2 раза сделать следующий комплекс: 25 подъемов ног в висе и 25 прямых скручиваний.'),
        ],
      ),
      new ListFunctionTraining(
        'Далее необходимо продолжать тренировки по такой же схеме, однако каждый раз увеличивать нагрузки: брать больший вес или выполнять большее количество повторений.',
      ),
    ],
  ),
  new ListFunctionTraining(
    'Программа функциональной тренировки для женщин.',
    <ListFunctionTraining>[
      new ListFunctionTraining(
        'Каждая тренировка длится не менее часа и начинается с разминки. Далее выполняются комплексы упражнений.',
      ),
      new ListFunctionTraining(
        'Понедельник',
        <ListFunctionTraining>[
          new ListFunctionTraining(
            'Начинается с кардио — 10 минут. Далее следует комплекс упражнений:',
          ),
          new ListFunctionTraining('1. 5 подтягиваний.'),
          new ListFunctionTraining('2. 12 отжиманий.'),
          new ListFunctionTraining('3. 12 приседаний без веса.'),
          new ListFunctionTraining('Коплекс выполнять по кругу в течение 10 минут, далее делать перерыв на 1 минуту и заходить на следующий круг.'),
          
        ],
      ),
      new ListFunctionTraining(
        'Вторник',
        <ListFunctionTraining>[
          new ListFunctionTraining('Начинается с кардиоразминки — 15 минут. Затем нужно выполнить следующий комплекс:'),
          new ListFunctionTraining('1. 15 выпадов на каждую ногу.'),
          new ListFunctionTraining(
              '2. 15 отжиманий.'),
               new ListFunctionTraining(
              '3. 10 выпадов.'),
               new ListFunctionTraining(
              '4. 10 отжиманий.'),
               new ListFunctionTraining(
              '5. 7 выпадов.'),
               new ListFunctionTraining(
              '6. 7 отжиманий.'),
              new ListFunctionTraining(
              'Круг необходимо повторить пять раз, время на отдых между кругами — минута.'),
        ],
      ),
      new ListFunctionTraining(
        'Среда',
        <ListFunctionTraining>[
          new ListFunctionTraining('Начинается с разминки суставов, которая занимает от 5 до 7 минут. Далее необходимо выполнить два круга упражнений.'),
          new ListFunctionTraining('1. Первый круг — минута кросс-бег, минута отдыха. Повторить пять раз.',<ListFunctionTraining>[
            new ListFunctionTraining(
              'Кросс-бег – бег в ускоренном режиме с поворотами.',
            )
          ]),
          new ListFunctionTraining('2. Второй круг — 10 отжиманий, 7 выпадов, 7 зашагиваний на степ или скамью. Повторить пять раз с отдыхом 60 секунд.'),
        ]
      ),
      new ListFunctionTraining(
        'Четверг',
        <ListFunctionTraining>[
          new ListFunctionTraining('Начинается с разминки суставов. Далее выполняется комплекс:'),
          new ListFunctionTraining(
              '1. 5 подходов становой тяги: первый подход — 10 повторений, второй — 10, третий — 7, четвертый — 7, пятый — 5.'),
          new ListFunctionTraining(
              '2. Выпады — 3 подхода по 15 повторений на каждую ногу.'),
          new ListFunctionTraining('3. Растяжка — 5-7 минут.'),
          ],
      ),
      
      new ListFunctionTraining(
        'Далее необходимо продолжать тренировки по такой же схеме.',
      ),
    ],
  ),
];
