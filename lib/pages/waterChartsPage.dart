import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import '../step_counter.dart';

class WaterChartsPage extends StatelessWidget {
  final int waterCounter;
  final List<charts.Series<MonthGlassOfWaterCharts, DateTime>>
      _seriespieDataWaterOfGlassMonth;
  final List<charts.Series<YearGlassOfWaterCharts, String>>
      _seriespieDataWaterOfGlassYear;
  final List<Tab> myTabs = [
    Tab(text: 'Информация'),
    Tab(text: 'Месяц'),
    Tab(text: 'Год')
  ];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  WaterChartsPage(this.waterCounter, this._seriespieDataWaterOfGlassMonth,
      this._seriespieDataWaterOfGlassYear);
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          bottom: TabBar(
            tabs: myTabs,
          ),
          title: Text('Вода'),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(FontAwesomeIcons.questionCircle),
                onPressed: () {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                    content: Text(
                      'Изучайте динамику количества выпитой вами воды, чтобы не допустить обезвоживание организма.',
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    duration: Duration(seconds: 5),
                    backgroundColor: Colors.orange,
                  ));
                })
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 250,
                      alignment: Alignment.center,
                      child: new Image(
                        image: AssetImage('assets/images/water-glass.png'),
                      ),
                    ),
                    Text(
                      '$waterCounter',
                      style: TextStyle(fontSize: 100),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 300,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding (padding: EdgeInsets.all(10.0), child :Text(
                        'Количество дней, когды вы выпивали норму воды в день. Следите за количество выпитой воды. 1 стакан содержит 250 мл.',
                        style: TextStyle(fontSize: 17),textAlign: TextAlign.justify,
                      ),),
                    )
                  ],
                )),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Месяц',
                      style: TextStyle(fontSize: 14),
                    ),
                    Expanded(
                      child: new charts.TimeSeriesChart(
                        _seriespieDataWaterOfGlassMonth,
                        animate: true,
                        defaultRenderer:
                            new charts.BarRendererConfig<DateTime>(),
                        defaultInteractions: false,
                        behaviors: [
                          new charts.SelectNearest(),
                          new charts.DomainHighlighter()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Год',
                      style: TextStyle(fontSize: 1),
                    ),
                    Expanded(
                      child: new charts.BarChart(
                        _seriespieDataWaterOfGlassYear,
                        animate: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
