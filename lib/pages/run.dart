import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RunPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Тренировка бега"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Программа тренировок начального уровня по бегу предназначена для людей, которые ведут сидячий образ жизни, проходят курс реабилитации после травмы, имеют физические ограничения или попросту испытывают удовольствие от бега в умеренном темпе. Кроме того, тренировочная программа начального уровня позволяет организму постепенно адаптироваться к физическим нагрузкам, предусмотренным обычной программой тренировок по бегу.',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
      ),
      body: new ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return new Training(listOfTiles[index]);
        },
        itemCount: listOfTiles.length,
      ),
    );
  }
}

class Training extends StatelessWidget {
  final RunningTaining runningTaining;
  Training(this.runningTaining);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(runningTaining);
  }

  Widget _buildTiles(RunningTaining root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<RunningTaining>(root),
      title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );
  }
}

class RunningTaining {
  String title;
  List<RunningTaining> children;
  RunningTaining(this.title, [this.children = const <RunningTaining>[]]);
}

List<RunningTaining> listOfTiles = <RunningTaining>[
  new RunningTaining(
      'Программа состоит из трех тренировок в неделю. Например, вы можете тренироваться по понедельникам, средам и пятницам (или же выбрать любые другие дни с учетом одного дня отдыха между тренировками).'),
  new RunningTaining(
      'Если длительное время у вас не было возможности регулярно бегать, то эта программа поможет безопасно восстановить форму и выйти на привычный уровень нагрузок'),
  new RunningTaining(
    'Неделя 1',
    <RunningTaining>[
      new RunningTaining('1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
      new RunningTaining(
          '2. Чередование ходьбы и бега на протяжении 20 минут:'),
      new RunningTaining('60 секунд бега трусцой'),
      new RunningTaining('90 секунд ходьбы.'),
    ],
  ),
  new RunningTaining(
    'Неделя 2',
    <RunningTaining>[
      new RunningTaining('1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
      new RunningTaining(
          '2. Чередование ходьбы и бега на протяжении 20 минут:'),
      new RunningTaining('90 секунд бега трусцой'),
      new RunningTaining('2 минуты ходьбы.'),
    ],
  ),
  new RunningTaining(
    'Неделя 3',
    <RunningTaining>[
      new RunningTaining('1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
      new RunningTaining('2. Бег трусцой 90 секунд'),
      new RunningTaining('3. Ходьба 90 секунд'),
      new RunningTaining('4. Бег трусцой 3 минуты'),
      new RunningTaining('5. Ходьба 3 минуты'),
      new RunningTaining('6. Бег трусцой 90 секунд'),
      new RunningTaining('7. Ходьба 90 секунд'),
      new RunningTaining('8. Бег трусцой 3 минуты'),
      new RunningTaining('9. Ходьба 3 минуты'),
    ],
  ),
  new RunningTaining(
    'Неделя 4',
    <RunningTaining>[
      new RunningTaining('1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
      new RunningTaining('2. Бег трусцой 3 минуты'),
      new RunningTaining('3. Ходьба 90 секунд'),
      new RunningTaining('4. Бег трусцой 5 минут'),
      new RunningTaining('5. Ходьба 3 минуты'),
      new RunningTaining('6. Бег трусцой 3 минуты'),
      new RunningTaining('7. Ходьба 90 секунд'),
      new RunningTaining('8. Бег трусцой 5 минут'),
    ],
  ),
  new RunningTaining(
    'Неделя 5',
    <RunningTaining>[
      new RunningTaining(
        '1-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 5 минут'),
          new RunningTaining('3. Ходьба 3 минуты'),
          new RunningTaining('4. Бег трусцой 5 минут'),
          new RunningTaining('5. Ходьба 3 минуты'),
          new RunningTaining('6. Бег трусцой 3 минуты'),
        ],
      ),
      new RunningTaining(
        '2-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 8 минут'),
          new RunningTaining('3. Ходьба 5 минут'),
          new RunningTaining('4. Бег трусцой 8 минут'),
        ],
      ),
      new RunningTaining(
        '3-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 20 минут'),
        ],
      ),
    ],
  ),
  new RunningTaining(
    'Неделя 6',
    <RunningTaining>[
      new RunningTaining(
        '1-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 5 минут'),
          new RunningTaining('3. Ходьба 3 минуты'),
          new RunningTaining('4. Бег трусцой 8 минут'),
          new RunningTaining('5. Ходьба 3 минуты'),
          new RunningTaining('6. Бег трусцой 5 минут'),
        ],
      ),
      new RunningTaining(
        '2-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 10 минут'),
          new RunningTaining('3. Ходьба 3 минуты'),
          new RunningTaining('4. Бег трусцой 10 минут'),
        ],
      ),
      new RunningTaining(
        '3-я тренировка:',
        <RunningTaining>[
          new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 25 минут'),
        ],
      ),
    ],
  ),
  new RunningTaining(
    'Неделя 7',
    <RunningTaining>[
      new RunningTaining(
        '1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 25 минут'),
    ],
  ),
  new RunningTaining(
    'Неделя 8',
    <RunningTaining>[
      new RunningTaining(
        '1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 28 минут'),
    ],
  ),
  new RunningTaining(
    'Неделя 9',
    <RunningTaining>[
      new RunningTaining(
        '1-я, 2-я и 3-я тренировка:'),
      new RunningTaining('1. 5 минут быстрой ходьбы.'),
          new RunningTaining('2. Бег трусцой 30 минут'),
    ],
  ),
];
