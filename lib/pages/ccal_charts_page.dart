import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import '../step_counter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CcalChartsPage extends StatelessWidget {
  final List<charts.Series<MonthCcalCharts, DateTime>> _seriespieDataCcalMonth;
  final List<charts.Series<YearCcalCharts, String>> _seriespieDataCcalYear;
  final List<Tab> myTabs = [Tab(text: 'Месяц'), Tab(text: 'Год')];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  CcalChartsPage(this._seriespieDataCcalMonth, this._seriespieDataCcalYear);
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          bottom: TabBar(
            tabs: myTabs,
          ),
          title: Text('Ккал'),
          actions: <Widget>[
          new IconButton(
              icon: new Icon(FontAwesomeIcons.questionCircle),
              onPressed: () {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Изучайте динамику сожжённых калорий, чтобы улучшить результаты.',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  duration: Duration(seconds: 5),
                  backgroundColor: Colors.orange,
                ));
              })
        ],
        ),
        body: TabBarView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Месяц',
                      style: TextStyle(fontSize: 14),
                    ),
                    Expanded(
                      child: new charts.TimeSeriesChart(
                        _seriespieDataCcalMonth,
                        animate: true,
                        defaultRenderer:
                            new charts.BarRendererConfig<DateTime>(),
                        defaultInteractions: false,
                        behaviors: [
                          new charts.SelectNearest(),
                          new charts.DomainHighlighter()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                width: 350,
                height: 250,
                child: Column(
                  children: <Widget>[
                    Text(
                      'Год',
                      style: TextStyle(fontSize: 14),
                    ),
                    Expanded(
                      child: new charts.BarChart(
                        _seriespieDataCcalYear,
                        animate: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
