package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import com.example.flutterpedometer.FlutterPedometerPlugin;
import io.flutter.plugins.pathprovider.PathProviderPlugin;
import cachet.plugins.pedometer.PedometerPlugin;
import com.tekartik.sqflite.SqflitePlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    FlutterPedometerPlugin.registerWith(registry.registrarFor("com.example.flutterpedometer.FlutterPedometerPlugin"));
    PathProviderPlugin.registerWith(registry.registrarFor("io.flutter.plugins.pathprovider.PathProviderPlugin"));
    PedometerPlugin.registerWith(registry.registrarFor("cachet.plugins.pedometer.PedometerPlugin"));
    SqflitePlugin.registerWith(registry.registrarFor("com.tekartik.sqflite.SqflitePlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
